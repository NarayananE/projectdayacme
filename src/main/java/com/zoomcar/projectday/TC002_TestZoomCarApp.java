package com.zoomcar.projectday;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.apache.poi.ss.formula.functions.Today;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

public class TC002_TestZoomCarApp extends Annotations {
	@BeforeClass
	public void setData() {
		testCaseName = "TC002_TestZoomCarApp";
		testCaseDes = "Test case to validate the Zoom Car Application";
		author = "Narayanan Elayedathumana";
		category = "Functional";
	}

	@Test
	public void testZoomCarApp() throws InterruptedException, IOException {
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		click(locateElement("link", "Start your wonderful journey"));
		click(locateElement("xpath", "(//div[text()='Popular Pick-up points']/following-sibling::div)[1]"));
		WebElement eleNextButton = locateElement("xpath", "//button[text()='Next']");
		verifyEnabled(eleNextButton);
		click(eleNextButton);
		int tomorrowsDate = getTomDay();
		click(locateElement("xpath", "//div[contains(text(),'" + tomorrowsDate + "')]"));
		WebElement eleNextButton2 = locateElement("xpath", "//button[text()='Next']");
		verifyEnabled(eleNextButton2);
		click(eleNextButton2);
		click(locateElement("xpath", "//div[contains(text(),'" + (tomorrowsDate + 5) + "')]"));
		click(locateElement("xpath", "//button[text()='Done']"));
		// locateElements("xpath", "//div[@class='details']/h3");
//		WebDriverWait wait = new WebDriverWait(driver, 10);
//		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElementsByXPath("//div[@class='details']/h3")));	
		Thread.sleep(10000);
		List<WebElement> noOfResults = locateElements("xpath", "//div[@class='component-car-item']");
		int resultCount = noOfResults.size();
		System.out.println("Number of Results is" + resultCount);
		List<WebElement> priceList = locateElements("xpath", "//div[@class='price']");
		List<Integer> pricInInLIst = new ArrayList<>();
		for (WebElement echPrice : priceList) {
			String price = echPrice.getText();
			// System.out.println(price);
			price = price.replaceAll("[^0-9]", "");
			System.out.println(price);
			pricInInLIst.add(Integer.parseInt(price));
		}
		int maxPrice = Collections.max(pricInInLIst);
		String maxPriceString = NumberFormat.getNumberInstance(Locale.US).format(maxPrice);
		System.out.println("Maximum Price: " + maxPriceString);		
		WebElement eleBrandName = locateElement("xpath", "//div[contains(text(),'" +maxPriceString+ "' )]/../../preceding::h3");
		String HighBrandName = eleBrandName.getText();
		System.out.println("Brand Name with Highest Price: "+HighBrandName+". And the Price is: "+maxPriceString);		
		WebElement bookingButton = locateElement("xpath", "(//div[contains(text(),'" +maxPriceString+ "' )]/following::button)[1]");				
		click(bookingButton);
	}

	public int getTomDay() {
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String toddyDate = sdf.format(date);
		int tomorrowDate = (Integer.parseInt(toddyDate)) + 1;
		System.out.println(tomorrowDate);
		return tomorrowDate;
	}
}
