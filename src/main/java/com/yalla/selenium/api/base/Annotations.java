package com.yalla.selenium.api.base;

import org.testng.annotations.Test;

import utils.ConfigReader;
import utils.DataLibrary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase {
	
	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		Object[][] dataSet = DataLibrary.readData(fileName);
		
		/*Object[][] dataSet = new Object[2][3];
		dataSet[0][0] = "BNYM";
		dataSet[0][1] = "Narayanan";
		dataSet[0][2] = "E";
		
		dataSet[1][0] = "Paragon";
		dataSet[1][1] = "Revathy";
		dataSet[1][2] = "TC";
		
		return dataSet;*/
		return dataSet;		
	}

	@BeforeMethod(groups = {"any"})
	//@Parameters({"url", "username", "password"})
	public void beforeMethod() throws IOException {
		ConfigReader oConfig = new ConfigReader();
		String url = oConfig.getPropValue("url");
		String username = oConfig.getPropValue("username");
		String password = oConfig.getPropValue("password");	
		startApp("chrome", url);
		WebElement eleUser, elePassword, eleLoginButton;
		eleUser = locateElement("id", "username");
		elePassword = locateElement("id", "password");
		eleLoginButton = locateElement("class", "decorativeSubmit");
		clearAndType(eleUser, username);
		clearAndType(elePassword, password);
		click(eleLoginButton);
		// System.out.println(driver.getTitle());
		if (verifyTitle("TestLeaf Automation")) {
			System.out.println("The page title is verified and correct");
		} else {
			System.out.println("Page title is not correct");
		}
	}

	@AfterMethod(groups = {"any"})
	public void afterMethod() {
		quit();
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}

	@AfterSuite
	public void afterSuite() {
	}

	
}
