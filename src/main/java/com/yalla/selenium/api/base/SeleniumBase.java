package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import utils.Reports;

public class SeleniumBase extends Reports implements Browser, Element {

	public RemoteWebDriver driver;
	WebDriverWait wait;
	int i = 1;

	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element " + ele + " clicked", "pass");
			// System.out.println("The Element " + ele + " clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element " + ele + " could not be clicked " + e, "fail");
			// System.err.println("The Element " + ele + " could not be clicked "+e);
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception"+e);
		} finally {
			takeSnap();
		}
	}

	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(ele.getAttribute("value").concat(data));
			reportStep("The Data :" + data + " Appended Successfully", "pass");
			// System.out.println("The Data :" + data + " Appended Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element " + ele + " is not Interactable " + e, "fail");
			// System.err.println("The Element " + ele + " is not Interactable "+e);
			throw new RuntimeException();
		} catch (IllegalArgumentException e) {
			reportStep("The Element " + ele + " Or Data " + data + " is Not Valid" + e, "fail");
			// System.err.println("The Element " + ele + " Or Data "+data+" is Not
			// Valid"+e);
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception"+e);
		} finally {
			takeSnap();
		}
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			reportStep("The Data cleared Successfully", "pass");
			// System.out.println("The Data cleared Successfully");
		} catch (InvalidElementStateException e) {
			reportStep("The element is not user-editable " + e, "fail");
			// System.err.println("The element is not user-editable "+e);
		} catch (IllegalArgumentException e) {
			reportStep("The Element " + ele + "is Not Valid" + e, "fail");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception"+e);
		} finally {
			takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :" + data + " entered Successfully", "pass");
			// System.err.println("The Data :" + data + " entered Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element " + ele + " is not Interactable", "fail");
			// System.err.println("The Element " + ele + " is not Interactable");
			throw new RuntimeException();
		} catch (IllegalArgumentException e) {
			reportStep("The Element " + ele + "is Not Valid" + e, "fail");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		String eleText = null;
		try {
			eleText = ele.getText();
			reportStep("The Text " + eleText + " Retrieved successfully from Element " + ele, "pass");
			return eleText;
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception"+e);
		} finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		try {
			reportStep("The background-color Retrieved successfully from Element " + ele, "pass");
			return ele.getCssValue("background-color");

		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception"+e);
		} finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		try {
			reportStep("The Attribute Retrieved successfully from Element " + ele, "pass");
			return ele.getAttribute("value");
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception"+e);
		} finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dropDown1 = new Select(ele);
			dropDown1.selectByVisibleText(value);
			reportStep("The DropDown item " + value + " has been Selected", "pass");
			// System.out.println("The DropDown item " + value + " has been Selected");
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Or the Option" + value + "is Not Present", "fail");
			// System.err.println("The Element " + ele + " Or the Option" + value + "is Not
			// Present");
			// System.err.println("The Element with locator:"+locatorType+" Not Found with
			// value: "+value);
			throw new RuntimeException();
		} catch (UnexpectedTagNameException e) {
			reportStep("The Tag Name for the WebElement is other than Tag Name: " + e, "fail");
			// System.err.println("The Tag Name for the WebElement is other than Tag Name:
			// "+e);
			// e.printStackTrace();
		} catch (Exception e2) {
			reportStep("Unknown Exception" + e2, "fail");
			// System.err.println("Unknown Exception" + e2);
		} finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dropDown1 = new Select(ele);
			dropDown1.selectByIndex(index);
			reportStep("The DropDown index " + index + " has been Selected", "pass");
			// System.out.println("The DropDown index " + index + " has been Selected");
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Or the Option" + index + "is Not Present", "fail");
			// System.err.println("The Element " + ele + " Or the Option" + index + "is Not
			// Present");
			// System.err.println("The Element with locator:"+locatorType+" Not Found with
			// value: "+value);
			throw new RuntimeException();
		} catch (UnexpectedTagNameException e) {
			reportStep("The Tag Name for the WebElement is other than Tag Name: " + e, "fail");
			System.err.println("The Tag Name for the WebElement is other than Tag Name: " + e);
			// e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			reportStep("The Index given is beyond the limit of drop down" + e, "fail");
			// System.err.println("The Index given is beyond the limit of drop down"+e);
		} catch (Exception e2) {
			reportStep("Unknown Exception" + e2, "fail");
			// System.err.println("Unknown Exception" + e2);
		} finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select dropDown1 = new Select(ele);
			dropDown1.selectByValue(value);
			reportStep("The DropDown value " + value + " has been Selected", "pass");
			// System.out.println("The DropDown value " + value + " has been Selected");
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Or the Option value" + value + "is Not Present", "fail");
			// System.err.println("The Element " + ele + " Or the Option value" + value +
			// "is Not Present");
			// System.err.println("The Element with locator:"+locatorType+" Not Found with
			// value: "+value);
			throw new RuntimeException();
		} catch (UnexpectedTagNameException e) {
			reportStep("The Tag Name for the WebElement is other than Tag Name: " + e, "fail");
			System.err.println("The Tag Name for the WebElement is other than Tag Name: " + e);
			// e.printStackTrace();
		} catch (Exception e2) {
			reportStep("Unknown Exception", "fail");
			// System.err.println("Unknown Exception" + e2);
		} finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {

		try {
			String actualText = ele.getText();
			if (actualText.equals(expectedText)) {
				reportStep(actualText + " is exactly Similar to " + expectedText + " Verified", "pass");
				return true;
			} else {
				reportStep(actualText + " is not exactly Similar to " + expectedText + " Verified", "pass");
				return false;
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {

		try {
			String actualText = ele.getText();
			if (actualText.contains(expectedText)) {
				reportStep(actualText + " is partially Similar to " + expectedText + " Verified", "pass");
				return true;
			} else {
				reportStep(actualText + " is not partially Similar to " + expectedText + " Verified", "pass");
				return false;
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {

		try {
			String actualValue = ele.getAttribute(attribute);
			if (actualValue.equals(value)) {
				reportStep(actualValue + " is exactly Similar to " + value + " Verified", "pass");
				return true;
			} else {
				reportStep(actualValue + " is not exactly Similar to " + value + " Verified", "pass");
				return false;

			}
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String actualValue = ele.getAttribute(attribute);
			if (actualValue.contains(value)) {
				reportStep(actualValue + " is partially Similar to " + value + " Verified", "pass");
				return true;
			} else {
				reportStep(actualValue + " is not partially Similar to " + value + " Verified", "pass");
				return false;

			}
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {

		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(ele));
			if (ele.isDisplayed()) {
				reportStep("The Element " + ele + " is Displayed", "pass");
				return true;
			} else {
				reportStep("The Element " + ele + " is not Displayed", "pass");
				return false;
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOf(ele));
			if (!ele.isDisplayed()) {
				reportStep("The Element " + ele + " is Disappeared", "pass");
				return true;
			} else {
				reportStep("The Element " + ele + " is not Disappeared", "pass");
				return false;
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element " + ele + " Not Found ", "fail");
			// System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();

		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(ele));
			if (ele.isEnabled()) {
				reportStep("The Element " + ele + " is Enabled", "pass");
				return true;
			} else {
				reportStep("The Element " + ele + " is not Displayed", "pass");
				return false;
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeSelected(ele));
			if (ele.isSelected()) {
				reportStep("The Element " + ele + " is Enabled", "pass");
				return true;
			} else {
				reportStep("The Element " + ele + " is not Displayed", "pass");
				return false;
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element " + ele + " Not Found ");
			throw new RuntimeException();
		} catch (Exception e) {
			System.err.println("Unknown Exception" + e);
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public void startApp(String url) {
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.get(url);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Chrome Browser Started, Maximized, deleted all cookies and URL Launched", "pass");
		} catch (Exception e) {
			reportStep("The Chrome Browser Could not be Launched. Hence Failed" + e, "fail");
			// System.err.println("The Browser Could not be Launched. Hence Failed" + e);
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Browser" + browser + "  Started, Maximized, deleted all cookies and URL Launched", "pass");
		} catch (Exception e) {
			reportStep("The Browser" + browser + " Could not be Launched. Hence Failed" + e, "fail");
			// System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) throws IOException {
		try {
			switch (locatorType.toLowerCase()) {
			case "id":
				return driver.findElementById(value);
			case "name":
				return driver.findElementByName(value);
			case "class":
				return driver.findElementByClassName(value);
			case "link":
				return driver.findElementByLinkText(value);
			case "xpath":
				return driver.findElementByXPath(value);
			case "partiallink":
				System.out.println(value);
				return driver.findElement(By.partialLinkText(value));
			// reportStep("Element Identified with Locator type as "+ locatorType+"and value
			// as "+value, "pass");
			}
		} catch (NoSuchElementException e) {
			String path = takeSnap();
			reportStep("The Element with locator:" + locatorType + " Not Found with value: ", "fail", path);
			//reportStep("The Element with locator:" + locatorType + " Not Found with value: " + value, "fail");
			// System.err.println("The Element with locator:" + locatorType + " Not Found
			// with value: " + value);
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			return driver.findElement(By.id(value));
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator id:" + value + " Not Found" + e, "fail");
			// System.err.println("The Element with locator id:" + value + " Not Found" +
			// e);
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch (type.toLowerCase()) {
			case "id":
				return driver.findElementsById(value);
			case "name":
				return driver.findElementsByName(value);
			case "class":
				return driver.findElementsByClassName(value);
			case "link":
				return driver.findElementsByLinkText(value);
			case "xpath":
				return driver.findElementsByXPath(value);
			case "partialLink":
				return driver.findElements(By.partialLinkText(value));
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator:" + type + " Not Found with value: " + value, "fail");
			// System.err.println("The Element with locator:" + type + " Not Found with
			// value: " + value);
			throw new RuntimeException();
		}

		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			reportStep("Switched to Alert", "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There are no Alert Present" + e, "fail");
			// System.out.println("There are no Alert Present" + e);
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			reportStep("Alert Accepted", "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There are no Alert Present" + e, "fail");
			// System.err.println("There are no Alert Present" + e);
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Alert Dismissed", "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There are no Alert Present" + e, "fail");
			// System.err.println("There are no Alert Present" + e);
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		}
	}

	@Override
	public String getAlertText() {
		try {
			return driver.switchTo().alert().getText();
		} catch (NoAlertPresentException e) {
			reportStep("There are no Alert Present" + e, "fail");
			// System.err.println("There are no Alert Present" + e);
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			reportStep("Text typed in Alert Box's Text Box", "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There are no Alert Present" + e, "fail");
			// System.err.println("There are no Alert Present" + e);
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWin = driver.getWindowHandles();
			List<String> listWin = new ArrayList<String>(allWin);
			String value = listWin.get(index);
			driver.switchTo().window(value);
			reportStep("Switched to " + index + " number window ", "pass");
		} catch (NoSuchWindowException e) {
			reportStep("The Window to be switched does not exist", "fail");
			// System.out.println("The Window to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}
	}

	@Override
	public void switchToWindow(String title) {
		try {
			Set<String> allWin = driver.getWindowHandles();
			List<String> listWin = new ArrayList<String>(allWin);
			for (String eachWin : listWin) {
				driver.switchTo().window(eachWin);
				if (driver.getTitle().equalsIgnoreCase(title)) {
					reportStep("Switched to window with title" + title, "pass");
					break;
				}
			}
		} catch (NoSuchWindowException e) {
			reportStep("The Window to be switched does not exist", "fail");
			// System.err.println("The Window to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.err.println("Unknown Exception" + e);
		}
	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			reportStep("Switched to " + index + " number frame ", "pass");
		} catch (NoSuchFrameException e) {
			reportStep("The Frame to be switched does not exist", "fail");
			// System.out.println("The Frame to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("Switched to frame as WebElement " + ele, "pass");
		} catch (NoSuchFrameException e) {
			reportStep("The Frame to be switched does not exist", "fail");
			// System.out.println("The Frame to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			reportStep("Switched to frame with Id or Name as  " + idOrName, "pass");
		} catch (NoSuchFrameException e) {
			reportStep("The Frame to be switched does not exist", "fail");
			// System.out.println("The Frame to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}
	}

	@Override
	public void defaultContent() {

		try {
			driver.switchTo().defaultContent();
			reportStep("Switched to defaul frame", "pass");

		} catch (NoSuchFrameException e) {
			reportStep("The Frame to be switched does not exist", "fail");
			// System.out.println("The Frame to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}

	}

	@Override
	public boolean verifyUrl(String url) {
		String actualURL = driver.getCurrentUrl();
		if (actualURL.contains(url)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean verifyTitle(String title) {		
		String actualTitle = driver.getTitle();
		if (actualTitle.contains(title)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String takeSnap() {
		String path=null, path1=null;
		try {

			File source = driver.getScreenshotAs(OutputType.FILE);
			path1 ="./snapShots/snap" + i + ".png";
			path = "E:\\TestLeaf\\FrameWork_Yalla\\Yalla\\snapShots\\snap_" + System.currentTimeMillis() + ".png";
			File des = new File(path1);
			FileUtils.copyFile(source, des);
			i++;
			reportStep("Screen shot has been taken for the step", "pass");
			return path;
			
		} catch (WebDriverException e) {
			reportStep("Unable to take the Screen shot due to: " + e, "fail");
			// System.err.println("Unable to take the Screen shot due to: " + e);

		} catch (IOException e) {
			reportStep("Unable to Take the Screen shots due to: " + e, "fail");
			// System.err.println("Unable to Take the Screen shots due to: " + e);
			// e.printStackTrace();
		}
		return null;

	}

	@Override
	public void close() {
		driver.close();
		reportStep("The Browser has been closed", "pass");

	}

	@Override
	public void quit() {
		driver.quit();
		reportStep("The Browser has been quit", "pass");

	}

	@Override
	public String getCurrentWindow() {

		try {
			return driver.getWindowHandle();

		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}
		return null;
	}

	@Override
	public void switchToDefaultWindow(String value) {
		try {
			switchToWindow(value);
			reportStep("Switched to " + value + " window ", "pass");
		} catch (NoSuchWindowException e) {
			reportStep("The Window to be switched does not exist", "fail");
			// System.out.println("The Window to be switched does not exist");
		} catch (Exception e) {
			reportStep("Unknown Exception" + e, "fail");
			// System.out.println("Unknown Exception" + e);
		}

	}

}
