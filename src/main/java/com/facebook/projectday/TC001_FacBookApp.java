package com.facebook.projectday;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;


public class TC001_FacBookApp extends Annotations {
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_FacBookApp";
		testCaseDes = "Test case to validate the Facebook APplication";
		author = "Narayanan Elayedathumana";
		category = "Functional";
	}
	@Test
	public void startFbApp() throws IOException {		
		startApp("https://www.facebook.com/");
		clearAndType(locateElement("email"), "narayanan.elayidam@gmail.com");
		clearAndType(locateElement("pass"), "APvn020690");
		click(locateElement("xpath", "//input[@value='Log In']"));
		Actions build = new Actions(driver);
		build.sendKeys(Keys.ESCAPE).build().perform();
		//dismissAlert();
		WebElement searchBox = locateElement("xpath", "(//input[@placeholder='Search'])[1]");
		clearAndType(searchBox, "TestLeaf");
		click(locateElement("xpath", "//button[@aria-label='Search']"));
		build.sendKeys(Keys.ESCAPE).build().perform();
		WebElement eleCheckTestLeaf = locateElement("xpath", "((//div[text()='Places'])[2]/following::a[text()='TestLeaf'])[1]");
		verifyExactText(eleCheckTestLeaf, "TestLeaf");
		WebElement eleLikeButton = locateElement("xpath", "(//button[text()='Like'])[1]");
		String likeButtonText = getElementText(eleLikeButton);
		System.out.println("the Text Present in like Button is: "+likeButtonText);
		if(likeButtonText.equals("Liked")) {
			System.out.println("The page is already Liked");
		}
		else {
			System.out.println("The page has not been Liked");
		}
		click(eleCheckTestLeaf);
		build.sendKeys(Keys.ESCAPE).build().perform();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(), 'people like this')]"))));
		if(verifyTitle("TestLeaf")) {
			System.out.println("The Page title verifeid and it contains TestLeaf");
		}
		else {
			System.out.println("The Page title verification Failed and it does not contain TestLeaf");
		}
		WebElement eleLabelLike = locateElement("xpath", "//div[contains(text(), 'people like this')]");
		String textLableLike = getElementText(eleLabelLike);
		int numOfLikes = Integer.parseInt(textLableLike.replaceAll("\\D", ""));
		System.out.println("Number of Likes is: "+numOfLikes);
		
	}

}
