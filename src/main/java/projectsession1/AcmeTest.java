package projectsession1;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class AcmeTest {
	static RemoteWebDriver driver;
	public static String sUrl = "https://acme-test.uipath.com/account/login";

	@BeforeMethod
	public static void invokeBrowserLaunchUrl() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(sUrl);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@BeforeMethod
	@Parameters({ "user", "password" })
	public static void login(String user, String password) {
		driver.findElement(By.id("email")).sendKeys(user);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.id("buttonLogin")).click();
	}

	@Test(dataProvider = "getData")
	public static void enterVendoraAndSearch(String taxID) {
		WebElement eleVendorsTab = driver.findElement(By.xpath("//button[text()=' Vendors']"));
		Actions builder = new Actions(driver);
		builder.moveToElement(eleVendorsTab).pause(2000).build().perform();
		WebElement eleSearchVendor = driver.findElement(By.xpath("//a[text()='Search for Vendor']"));
		builder.moveToElement(eleSearchVendor).click().build().perform();
		driver.findElement(By.id("vendorTaxID")).sendKeys(taxID);
		driver.findElement(By.id("buttonSearch")).click();
		String vendorName = driver.findElement(By.xpath("(//div[text()='Search Results']/following::td)[1]")).getText();
		System.out.println("The Vendor Name is: " + vendorName);
	}

	@DataProvider(name = "getData")
	public Object[][] createData() {
		Object[][] dataArray = new Object[3][1];
		dataArray[0][0] = "RO094782";
		dataArray[1][0] = "RO123456";
		dataArray[2][0] = "IT754893";

		return dataArray;
	}
	@AfterMethod
	public void closeApp() {
		driver.close();
	}

}
