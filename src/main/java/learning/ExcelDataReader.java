package learning;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataReader {
	static Object[][] dataArray;
	public static Object[][] readData(String filename) throws IOException {
		File file = new File("./data/"+filename+".xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet1 = wb.getSheetAt(0);
		int rowCount = sheet1.getLastRowNum();
		System.out.println("The Row count is: "+rowCount);
		for(int i=1; i<rowCount;i++) {
			XSSFRow oRow = sheet1.getRow(i);
			short cellCount = oRow.getLastCellNum();
			dataArray = new Object[rowCount][cellCount];
			for(int j =0; j<cellCount; j++) {
				XSSFCell oCell = oRow.getCell(j);
				CellType cellDataType = oCell.getCellType();
				if(cellDataType == CellType.NUMERIC) {
					long numCellValue = (long) oCell.getNumericCellValue();
					dataArray[i-1][j] = numCellValue;
					System.out.print(numCellValue+ " ");
					
				}
				else if (cellDataType == CellType.STRING) {
					String strCellValue = oCell.getStringCellValue();
					dataArray[i-1][j] = strCellValue;
					System.out.print(strCellValue+ " ");
					
					
				}
				
			}
			System.out.println();
		}
		
		wb.close();
		return dataArray;
	}

}
