package testcases2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;
import com.yalla.selenium.api.base.SeleniumBase;

public class TC003_DuplicateLead extends Annotations {	
	@BeforeTest
	public void setData() {
		 testCaseName = "TC003_DuplicateLead";
		 testCaseDes = "Test case to validate the Duplicate Lead Functionality";
		 author = "Narayanan Elayedathumana";
		 category = "Smoke";
	 }
	@Test
	public void duplicateLeads() throws InterruptedException, IOException {
		String leadID = "10117";
		WebElement eleCRMLink = locateElement("link", "CRM/SFA");
		click(eleCRMLink);
		WebElement eleLeadsLink = locateElement("link", "Leads");
		click(eleLeadsLink);
		click(locateElement("partiallink", "Find Lead"));		
		clearAndType(locateElement("name", "id"), leadID);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement idLink = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(idLink);
		WebElement dupLead = locateElement("link", "Duplicate Lead");
		click(dupLead);
		click(locateElement("xpath", "//input[@value='Create Lead']"));
		WebElement updatedCompName = locateElement("xpath", "(//span[text()='Company Name']/following::span)[1]");
		if(verifyPartialText(updatedCompName, leadID)) {
			System.out.println("Duplicate Functionality failed");
		}
		else {
			
			System.out.println("Duplicated Suceessfully");
		}
	}
	
}

