package testcases2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;
import com.yalla.selenium.api.base.SeleniumBase;

public class TC004_MergeLead extends Annotations {	
	@BeforeTest(groups = {"regression"})
	public void setData() {
		 testCaseName = "TC004_MergeLead";
		 testCaseDes = "Test case to validate the Merge Lead Functionality";
		 author = "Narayanan Elayedathumana";
		 category = "Smoke";
	 }
	//@Test(enabled =false)
	@Test(groups = "regression")
	public void mergeLeadIDs() throws InterruptedException, IOException {
		String leadID1 = "10105";
		String leadID2 = "10113";
		String currWindow = getCurrentWindow();
		WebElement eleCRMLink = locateElement("link", "CRM/SF");
		click(eleCRMLink);
		WebElement eleLeadsLink = locateElement("link", "Leads");
		click(eleLeadsLink);
		WebElement eleMergeLeadLink = locateElement("link", "Merge Leads");
		click(eleMergeLeadLink);
		WebElement eleLookUp1 = locateElement("xpath", "(//input[@id='partyIdFrom']/following::img)[1]");
		click(eleLookUp1);
		switchToWindow(1);
		WebElement eleTestID = locateElement("xpath", "//input[@name = 'id']");
		clearAndType(eleTestID, leadID1);
		WebElement eleFindLeads = locateElement("xpath", "(//*[text()='Find Leads'])[3]");
		click(eleFindLeads);
		WebElement eleLeadIDsel = locateElement("xpath", "(//a[@class='linktext'])[1]");
		click(eleLeadIDsel);
		switchToWindow(0);
		WebElement eleLookUp2 = locateElement("xpath", "(//input[@id='partyIdFrom']/following::img)[2]");
		click(eleLookUp2);
		switchToWindow(1);
		WebElement eleTestID2 = locateElement("xpath", "//input[@name = 'id']");
		clearAndType(eleTestID2, leadID2);
		WebElement eleLeadIDsel2 = locateElement("xpath", "(//a[@class='linktext'])[1]");
		click(eleLeadIDsel2);		
		switchToWindow(0);
		WebElement eleMergeButton = locateElement("xpath", "//a[text()='Merge']");
		click(eleMergeButton);
		switchToAlert();
		acceptAlert();
		WebElement eleFindLead = locateElement("link", "Find Leads");
		click(eleFindLead);
		WebElement eleLeadID = locateElement("name", "id");
		append(eleLeadID, leadID1);
		WebElement eleFindButton = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(eleFindButton);
		Thread.sleep(10000);
		WebElement bottomText = locateElement("xpath", "//div[@class ='x-paging-info']");
		if(verifyPartialText(bottomText, "No records to display")) {
			System.out.println("The Merge is successfull");
		}
		else {
			System.out.println("The Merge Failed");
		}

	}
}
