package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataWriter {
	
	public static void writeIntoExcel(String filePath, String sheetName, int rowNum, int cellNum, String cellVal) throws IOException {
		try {
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet oShleet = wb.getSheet(sheetName);
			if(oShleet == null) {
				wb.createSheet(sheetName);
			}
			XSSFRow oRow = oShleet.getRow(rowNum);
			if(oRow == null) {
				oShleet.createRow(rowNum);
				
			}
			XSSFCell oCell = oRow.getCell(cellNum);
			if(oCell == null) {
				oRow.createCell(cellNum);
			}
			
			oCell.setCellValue(cellVal);
			FileOutputStream fos = new FileOutputStream(file);
			wb.write(fos);
			wb.close();
		}
		catch(Exception e) {
			System.err.println("Exception Occured: "+e.getMessage());
		}
		
		
		
	}

}
