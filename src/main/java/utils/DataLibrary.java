package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataLibrary {

	public static Object[][] readData(String filename) throws IOException {
		File file = new File("./data/"+filename+".xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet1 = wb.getSheetAt(0);
		int rowCount = sheet1.getLastRowNum();
		System.out.println("The Row count is: "+rowCount);
		int cellCount = sheet1.getRow(0).getLastCellNum();
		System.out.println("The Row count is: "+cellCount);
		Object[][] dataArray = new Object[rowCount][cellCount];
		for(int i=1; i<=rowCount;i++) {
			XSSFRow oRow = sheet1.getRow(i);
			for(int j =0; j<cellCount; j++) {
				XSSFCell oCell = oRow.getCell(j);
				CellType cellDataType = oCell.getCellType();
				if (cellDataType == CellType.STRING) {
					String strCellValue = oCell.getStringCellValue();
					//System.out.print(strCellValue+ " ");
					dataArray[i-1][j] = strCellValue;
				}
				else if(cellDataType == CellType.NUMERIC) {
					long numCellValue = (long) oCell.getNumericCellValue();
					//System.out.print(numCellValue+ " ");
					dataArray[i-1][j] = numCellValue;
				}
			}
		}

		wb.close();
		return dataArray;
	}

}
