package utils;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
@Test
public class ReportExmp {
	public void report() throws IOException {
		File file = new File("./reports/result.html");
		ExtentHtmlReporter reporter = new ExtentHtmlReporter(file);	
		reporter.setAppendExisting(true); 
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		ExtentTest test = extent.createTest("MergeLEad", "Creating a new Lead");
		test.assignAuthor("Narayanan");
		test.assignCategory("Smoke");
		test.pass("MergeLead is successfull");
		test.fail("Merge is Failed", MediaEntityBuilder.createScreenCaptureFromPath("./../snapShots/snap1.png").build());
		extent.flush();
	}

}
