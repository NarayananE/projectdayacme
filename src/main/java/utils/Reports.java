package utils;


import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	public static ExtentHtmlReporter reporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	public String testCaseName, testCaseDes, author, category;
	public String fileName;
	
	//@BeforeSuite(groups = {"any"})
	public void startReport() {
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(reporter);
	}
	@BeforeClass(groups = {"any"})
	public void report() {
		test = extent.createTest(testCaseName, testCaseDes);
		test.assignAuthor(author);
		test.assignCategory(category);
		

	}
	public void reportStep(String des, String status) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(des);
		}
		else if(status.equalsIgnoreCase("fail")) {
			test.fail(des);
			//test.fail(des, MediaEntityBuilder.createScreenCaptureFromPath(path))
		}
		
	}
	public void reportStep(String des, String status, String path) {
		try {
			if(status.equalsIgnoreCase("pass")) {
				test.pass(des);
			}
			else if(status.equalsIgnoreCase("fail")) {
				//test.fail(des);
				test.fail(des, MediaEntityBuilder.createScreenCaptureFromPath(path).build());
			}
			
		}
		catch(Exception e) {
			System.out.println("Unknown Exception Ocuured: "+e);
		}
		
		
	}
	@AfterSuite(groups = {"any"})
	public void stopReport() {
		extent.flush();
	}

}
