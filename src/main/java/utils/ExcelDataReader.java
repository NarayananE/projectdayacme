package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataReader {
	
	public static void main(String args[]) throws IOException {
		Object [][] dataSet = null;
		String fileName = "createLead";
		File file = new File("./data/"+fileName+".xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet1 = wb.getSheetAt(0);
		int rowCount = sheet1.getLastRowNum();
		int cellCount = sheet1.getRow(0).getLastCellNum();
		dataSet = new Object[rowCount][cellCount];
		for(int i =1; i<=rowCount; i++) {
			XSSFRow oRow = sheet1.getRow(i);			
			for(int j=0; j<cellCount; j++) {
				XSSFCell oCell = oRow.getCell(j);	
				CellType oCellTypeEnum = oCell.getCellType();
				if(oCellTypeEnum == CellType.STRING) {
					dataSet[i-1][j]=oCell.getStringCellValue();
				}
				else if(oCellTypeEnum == CellType.NUMERIC){
					dataSet[i-1][j]=oCell.getNumericCellValue();
				}
				
				
			}
		}		
		System.out.println("Printing the values in the Object Array");
		for(int i=0; i<dataSet.length; i++) {
			for(int j=0; j<dataSet[i].length;j++) {
				System.out.print(dataSet[i][j]);
			}
			System.out.println();
		}
		
	}
	
	

}
