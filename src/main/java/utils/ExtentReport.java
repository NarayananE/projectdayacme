package utils;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {
	public static ExtentHtmlReporter reporter;
	public static ExtentReports extent;
	
	@BeforeSuite
	public void startReport() {
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(reporter);	
	}
	@Test
	public void report() throws IOException {
		ExtentTest test = extent.createTest("MergeLead", "This test case tests the Merge Test Functionality");
		test.assignAuthor("Narayanan");
		test.assignCategory("Smoke");
		test.pass("MergeLead is successfull");
		test.fail("Merge is Failed", MediaEntityBuilder.createScreenCaptureFromPath("./../snapShots/snap1.png").build());
	}
	@AfterSuite
	public void stopReport() {
		extent.flush();
	}
	
	

}
