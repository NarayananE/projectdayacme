package testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;
import com.yalla.selenium.api.base.SeleniumBase;

public class TC002_EditLead extends Annotations {	
	@BeforeTest(groups = {"sanity"})
	public void setData() {
		 testCaseName = "TC002_EditLead";
		 testCaseDes = "Test case to validate the Edit Lead Functionality";
		 author = "Narayanan Elayedathumana";
		 category = "Smoke";
	 }
	//@Test(timeOut=10000)
	//@Test(groups = {"sanity"}, dependsOnGroups = {"smoke"})	
	@Test(dependsOnMethods = {"testcases.TC001_CreateLead.createLead"})
	public void editLeads() throws InterruptedException, IOException {
		String leadID = "10006";
		WebElement eleCRMLink = locateElement("link", "CRM/SFA");
		click(eleCRMLink);
		WebElement eleLeadsLink = locateElement("link", "Leads");
		click(eleLeadsLink);
		click(locateElement("partiallink", "Find Lead"));
		clearAndType(locateElement("name", "id"), leadID);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement idLink = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(idLink);
		WebElement editButton = locateElement("link", "Edit");
		click(editButton);
		WebElement compName = locateElement("id", "updateLeadForm_companyName");
		String newCompName = "CTS";
		clearAndType(compName, newCompName);
		click(locateElement("xpath", "//input[@class = 'smallSubmit' and @value ='Update']"));
		WebElement findLead = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLead);		
		clearAndType(locateElement("name", "id"), leadID);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement idLink2 = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(idLink2);
		//Thread.sleep(10000);
		WebElement updatedCompName = locateElement("xpath", "(//span[text()='Company Name']/following::span)[1]");
		if(verifyPartialText(updatedCompName, newCompName)) {
			System.out.println("Modified or Edited Suceessfully");
		}
		else {
			System.out.println("The Modify was failed");
		}
	}
}
