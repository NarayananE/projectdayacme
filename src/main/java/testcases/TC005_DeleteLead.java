package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

public class TC005_DeleteLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		 testCaseName = "TC005_DeleteLead";
		 testCaseDes = "Test case to validate the Delete Lead Functionality";
		 author = "Narayanan Elayedathumana";
		 category = "Smoke";
	 }	
	@Test
	public void deleteLead() throws IOException {
		String leadID = "10097";
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		WebElement eleleadID = locateElement("name", "id");
		clearAndType(eleleadID, leadID);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		click(locateElement("xpath", "(//a[@class=\"linktext\"])[4]"));
		click(locateElement("link", "Delete"));
		click(locateElement("link", "Find Leads"));
		WebElement eleleadID2 = locateElement("name", "id");
		clearAndType(eleleadID2, leadID);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement bottomText = locateElement("xpath", "//div[@class ='x-paging-info']");
		if(verifyPartialText(bottomText, "No records to display")) {
			System.out.println("Lead successfully Deleted");
		}
		else {
			System.out.println("Lead Deletion Failed");
		}

		
	}
}
