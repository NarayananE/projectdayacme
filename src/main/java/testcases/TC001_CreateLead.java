package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

import utils.DataLibrary;

public class TC001_CreateLead extends Annotations{
	 
	@BeforeTest(groups = {"smoke"})
	public void setData() {
		 testCaseName = "TC001_CreateLead";
		 testCaseDes = "Test case to validate the Create Lead Functionality";
		 author = "Narayanan Elayedathumana";
		 category = "Smoke";
		 fileName = "createLead";
	 }
	@Test(invocationCount =2, groups = {"smoke"}, dataProvider = "fetchData")
	public void createLead(String comName, String fName, String lName) throws IOException {	
		WebElement eleCRMLink = locateElement("link", "CRM/SFA");
		click(eleCRMLink);
		click(locateElement("link", "Create Lead"));
		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompName, comName );
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, fName);
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLastName, lName);
		WebElement sourceDropDown = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(sourceDropDown, "Conference");
		WebElement eleCreateLeadButt = locateElement("xpath", "//*[@value='Create Lead']");
		click(eleCreateLeadButt);
	}
	
}







